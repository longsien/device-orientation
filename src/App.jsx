import { useRef } from 'react'
import './App.scss'

function App() {
  const ref = useRef()
  const mazeRef = useRef()

  const start = () => {
    // canvas
    const canvas = ref.current
    const mazeCanvas = mazeRef.current
    const ctx = canvas.getContext('2d')
    const mazeCtx = mazeCanvas.getContext('2d')
    const w = canvas.width
    const h = canvas.height

    const ps = Math.floor(w / 6)
    const br = Math.floor(ps / 4)
    const wt = 1

    let counter = 0

    // ----
    // maze

    const maze = []
    const path = []

    const startPos = {
      x: Math.floor((Math.random() * w) / ps),
      y: Math.floor((Math.random() * h) / ps),
    }

    const generateMaze = () => {
      for (let y = 0; y < h / ps - 1; y++) {
        maze.push([])

        for (let x = 0; x < w / ps - 1; x++) {
          maze[y].push({ coords: { x, y } })
        }
      }

      const getAdjacent = (block) => {
        const x = block.coords.x
        const y = block.coords.y

        const adjacent = [
          maze[y - 1]?.[x],
          maze[y]?.[x + 1],
          maze[y + 1]?.[x],
          maze[y]?.[x - 1],
        ]

        return adjacent
      }

      const expand = (block) => {
        block.counter = counter
        counter += 1
        const neighbours = getAdjacent(block)
        const mappedNeighbours = neighbours.map((item, i) => {
          if (item === undefined) return null
          if (item.exits) return null
          return i
        })
        const validNeighbours = mappedNeighbours.filter((item) => item !== null)
        const direction =
          validNeighbours[Math.floor(Math.random() * validNeighbours.length)]

        if (validNeighbours.length) {
          if (neighbours[direction]) {
            block.exits
              ? block.exits.push(direction)
              : (block.exits = [direction])

            neighbours[direction].exits = [
              direction === 1 ? 3 : Math.abs(direction - 2),
            ]

            expand(neighbours[direction])
          }
        } else {
          if (counter < 20) {
            counter = 0
            maze.length = 0
            generateMaze()
          }
        }
      }

      expand(maze[startPos.y][startPos.x])
    }

    generateMaze()

    // draw maze
    const drawMaze = () => {
      const flatMaze = maze.flat()
      flatMaze.forEach((item) => {
        if (item.exits) path.push(item)
      })

      mazeCtx.fillStyle = '#000'
      mazeCtx.fillRect(0, 0, w, h)

      path.forEach((item) => {
        const x = item.coords.x * ps
        const y = item.coords.y * ps

        setTimeout(() => {
          mazeCtx.clearRect(x + wt, y + wt, ps - wt * 2, ps - wt * 2)

          item.exits.forEach((exit) => {
            if (exit === 0) mazeCtx.clearRect(x + wt, y, ps - wt * 2, wt)

            if (exit === 1)
              mazeCtx.clearRect(x + ps - wt, y + wt, wt, ps - wt * 2)

            if (exit === 2)
              mazeCtx.clearRect(x + wt, y + ps - wt, ps - wt * 2, wt)

            if (exit === 3) mazeCtx.clearRect(x, y + wt, wt, ps - wt * 2)
          })
        }, item.counter * 10)
      })
    }

    drawMaze()

    // ----
    // ball
    const pos = { x: startPos.x * ps + ps / 2, y: startPos.y * ps + ps / 2 }
    const v = { x: 0, y: 0 }

    // sensor
    const o = { x: 0, y: 0 }

    if (
      DeviceMotionEvent &&
      typeof DeviceMotionEvent.requestPermission === 'function'
    ) {
      DeviceMotionEvent.requestPermission()
    }

    window.addEventListener(
      'deviceorientation',
      (e) => {
        o.x = e.gamma
        o.y = e.beta

        if (o.x > ps) o.x = ps
        if (-o.x < -ps) o.x = -ps
        if (o.y > ps) o.y = ps
        if (o.y < -ps) o.y = -ps
      },

      true
    )

    // render

    let prevPath = path.find(
      (item) =>
        item.coords.x === Math.floor(pos.x / ps) &&
        item.coords.y === Math.floor(pos.y / ps)
    )

    let jumped = false

    const tick = () => {
      requestAnimationFrame(tick)

      // update velocity
      v.x += o.x / br
      v.y += o.y / br

      if (Math.abs(v.x) > Math.abs(o.x)) v.x = o.x
      if (Math.abs(v.y) > Math.abs(o.y)) v.y = o.y

      // update position
      pos.x += v.x
      pos.y += v.y

      // collision check
      const insidePath = path.find(
        (item) =>
          item.coords.x === Math.floor(pos.x / ps) &&
          item.coords.y === Math.floor(pos.y / ps)
      )

      const bx = pos.x
      const by = pos.y

      const ppx = prevPath.coords.x * ps
      const ppy = prevPath.coords.y * ps

      // inside path

      if (insidePath) {
        const pathItem = insidePath

        const px = pathItem.coords.x * ps
        const py = pathItem.coords.y * ps

        if (pathItem.exits.indexOf(0) === -1) {
          if (prevPath.coords.y === pathItem.coords.y - 1) {
            v.y = v.y / -2
            pos.y = ppy + br + wt
            jumped = true
          } else if (by - br < py + wt) {
            v.y = v.y / -2
            pos.y = py + br + wt
            jumped = false
          }
        }

        if (pathItem.exits.indexOf(1) === -1) {
          if (prevPath.coords.x === pathItem.coords.x + 1) {
            v.x = v.x / -2
            pos.x = ppx + ps - br - wt
            jumped = true
          } else if (bx + br > px + ps - wt) {
            v.x = v.x / -2
            pos.x = px + ps - br - wt
            jumped = false
          }
        }

        if (pathItem.exits.indexOf(2) === -1) {
          if (prevPath.coords.y === pathItem.coords.y + 1) {
            v.y = v.y / -2
            pos.y = ppy + ps - br - wt
            jumped = true
          } else if (by + br > py + ps - wt) {
            v.y = v.y / -2
            pos.y = py + ps - br - wt
            jumped = false
          }
        }

        if (pathItem.exits.indexOf(3) === -1) {
          if (prevPath.coords.x === pathItem.coords.x - 1) {
            v.x = v.x / -2
            pos.x = ppx + br + wt
            jumped = true
          } else if (bx - br < px + wt) {
            v.x = v.x / -2
            pos.x = px + br + wt
            jumped = false
          }
        }

        prevPath = jumped ? prevPath : pathItem
      }

      if (!insidePath) {
        if (prevPath.exits.indexOf(0) === -1) {
          if (by - br < ppy + wt) {
            v.y = v.y / -2
            pos.y = ppy + br + wt
          }
        }

        if (prevPath.exits.indexOf(1) === -1) {
          if (bx + br > ppx + ps - wt) {
            v.x = v.x / -2
            pos.x = ppx + ps - br - wt
          }
        }

        if (prevPath.exits.indexOf(2) === -1) {
          if (by + br > ppy + ps - wt) {
            v.y = v.y / -2
            pos.y = ppy + ps - br - wt
          }
        }

        if (prevPath.exits.indexOf(3) === -1) {
          if (bx - br < ppx + wt) {
            v.x = v.x / -2
            pos.x = ppx + br + wt
          }
        }
      }

      if (pos.x < br) {
        pos.x = br
        v.x = v.x / -2
      }
      if (pos.x > w - br) {
        pos.x = w - br
        v.x = v.x / -2
      }
      if (pos.y < br) {
        pos.y = br
        v.y = v.y / -2
      }
      if (pos.y > h - br) {
        pos.y = h - br
        v.y = v.y / -2
      }

      // render ball
      ctx.fillStyle = '#fff'
      ctx.clearRect(0, 0, w, h)
      ctx.beginPath()
      ctx.arc(pos.x, pos.y, br, 0, Math.PI * 2)
      ctx.fill()
    }

    tick()
  }

  return (
    <div className='App'>
      <canvas
        className={'maze'}
        ref={mazeRef}
        width={window.innerWidth}
        height={window.innerHeight}
      />
      <canvas ref={ref} width={window.innerWidth} height={window.innerHeight} />

      <button onClick={start}>Start</button>
    </div>
  )
}

export default App
