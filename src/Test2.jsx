import { useRef } from 'react'
import './App.scss'

function App() {
  const ref = useRef()
  const mazeRef = useRef()

  const bs = Math.floor((window.innerWidth + window.innerHeight) / 25)
  const wt = 1

  const start = () => {
    // canvas

    const canvas = ref.current
    const ctx = canvas.getContext('2d')
    const w = canvas.width
    const h = canvas.height

    const mazeCanvas = mazeRef.current
    const mazeCtx = mazeCanvas.getContext('2d')

    // generate maze

    const bw = w / bs
    const bh = h / bs
    const maze = []
    const path = []

    let startPos

    const generateMaze = () => {
      for (let y = 0; y < bh; y++) {
        maze.push([])

        for (let x = 0; x < bw; x++) {
          maze[y].push({ coords: { x, y } })
        }
      }

      startPos =
        maze[Math.floor(Math.random() * bh)][Math.floor(Math.random() * bw)]

      startPos.step = 0
      let counter = 1

      // check if full

      const isFull = () => {
        return maze.every((row) => row.every((col) => col.exits?.length))
      }

      // get neighbours

      const getNeighbours = (block) => {
        const x = block.coords.x
        const y = block.coords.y

        return [
          maze[y - 1]?.[x],
          maze[y][x + 1],
          maze[y + 1]?.[x],
          maze[y][x - 1],
        ]
      }

      // expand path

      const expand = (block) => {
        let neighbours = getNeighbours(block)
        const processNeighbours = neighbours.map((item, i) => {
          if (item === undefined) return null
          if (item.exits) return null
          return i
        })
        const validNeighbours = processNeighbours.filter(
          (item) => item !== null
        )
        const direction =
          validNeighbours[Math.floor(Math.random() * validNeighbours.length)]

        if (
          neighbours.every((item) => {
            if (!item) return true
            if (item.exits?.length) return true

            return false
          })
        ) {
          counter += 1
          block.step = counter

          if (counter < 50) {
            maze.length = 0
            counter = 0
            generateMaze()
          } else {
            // if (!isFull()) {
            //   const flatMaze = maze.flat()
            //   const branchableBlocks = flatMaze.filter((item) => {
            //     if (item.exits?.length) {
            //       const hasValidNeighbours = getNeighbours(item).some(
            //         (n) => !n?.exits?.length
            //       )
            //       if (hasValidNeighbours) return true
            //     }
            //     return false
            //   })
            //   if (branchableBlocks.length) {
            //     console.log(branchableBlocks)
            //     // expand(branchableBlocks[0])
            //   }
            // }
          }
        } else {
          if (!neighbours[direction] || neighbours[direction].exits) {
            expand(block)
          } else {
            counter += 1
            block.step = counter

            block.exits
              ? block.exits.push(direction)
              : (block.exits = [direction])
            neighbours[direction].exits = [
              Math.abs(direction - 2 === -1 ? 3 : direction - 2),
            ]
            expand(neighbours[direction])
          }
        }
      }

      expand(startPos)

      // render maze

      mazeCtx.fillStyle = '#000'
      mazeCtx.fillRect(0, 0, w, h)

      path.length = 0

      maze.forEach((row) => {
        row.forEach((column) => {
          if (column.exits) {
            path.push(column)
          }
        })
      })

      path.forEach((item) => {
        setTimeout(() => {
          mazeCtx.clearRect(
            item.coords.x * bs + wt,
            item.coords.y * bs + wt,
            bs - wt * 2,
            bs - wt * 2
          )
          item.exits.forEach((exit) => {
            const clear = { x: 0, y: 0 }
            if (exit === 0) {
              clear.x = item.coords.x * bs + wt
              clear.y = item.coords.y * bs
            }

            if (exit === 1) {
              clear.x = item.coords.x * bs + wt * 2
              clear.y = item.coords.y * bs + wt
            }

            if (exit === 2) {
              clear.x = item.coords.x * bs + wt
              clear.y = item.coords.y * bs + wt * 2
            }

            if (exit === 3) {
              clear.x = item.coords.x * bs
              clear.y = item.coords.y * bs + wt
            }

            mazeCtx.clearRect(clear.x, clear.y, bs - wt * 2, bs - wt * 2)
          })
        }, item.step * 10)
      })
    }

    generateMaze()

    // ball

    const pos = {
      x: startPos.coords.x * bs + bs / 2,
      y: startPos.coords.y * bs + bs / 2,
    }

    const r = 10

    // sensors

    const o = { x: 0, y: 0 }

    if (
      DeviceOrientationEvent &&
      typeof DeviceOrientationEvent.requestPermission === 'function'
    ) {
      DeviceOrientationEvent.requestPermission()
    }

    window.addEventListener('deviceorientation', (e) => {
      o.x = e.gamma
      o.y = e.beta
    })

    // render ball

    const tick = () => {
      requestAnimationFrame(tick)

      // update position
      pos.x += o.x
      pos.y += o.y

      path.forEach((item) => {
        const px = pos.x
        const py = pos.y
        const bx = item.coords.x * bs
        const by = item.coords.y * bs

        if (px > bx && px < bx + bs && py > by && py < by + bs) {
          document.getElementById(
            'num'
          ).innerText = `[${item.coords.x}, ${item.coords.y}]`

          if (item.exits.indexOf(0) === -1) {
            if (py <= by + wt + r) pos.y = by + wt + r
          }

          if (item.exits.indexOf(1) === -1) {
            if (px >= bx + bs - wt - r) pos.x = bx + bs - wt - r
          }

          if (item.exits.indexOf(2) === -1) {
            if (py >= by + bs - wt - r) pos.y = by + bs - wt - r
          }

          if (item.exits.indexOf(3) === -1) {
            if (px <= bx + wt + r) pos.x = bx + wt + r
          }
        }
      })

      // draw

      ctx.fillStyle = '#fff'
      ctx.clearRect(0, 0, w, h)
      ctx.beginPath()
      ctx.arc(pos.x, pos.y, r, 0, Math.PI * 2)
      ctx.fill()
    }

    setTimeout(tick, 1000)
  }

  // render
  return (
    <div className='App'>
      <canvas
        className={'maze'}
        ref={mazeRef}
        width={Math.floor(window.innerWidth - (window.innerWidth % bs))}
        height={Math.floor(window.innerHeight - (window.innerHeight % bs))}
      />

      <canvas
        ref={ref}
        width={Math.floor(window.innerWidth - (window.innerWidth % bs))}
        height={Math.floor(window.innerHeight - (window.innerHeight % bs))}
      />

      <button onClick={start}>Start</button>
      <div id='num'></div>
    </div>
  )
}

export default App

// event = {alpha, beta, gamma}
