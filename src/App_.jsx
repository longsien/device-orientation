import { useRef } from 'react'
import './App.scss'

function App() {
  const ref = useRef()
  const mazeRef = useRef()

  const start = () => {
    // canvas
    const canvas = ref.current
    const mazeCanvas = mazeRef.current
    const ctx = canvas.getContext('2d')
    const mazeCtx = mazeCanvas.getContext('2d')
    const w = canvas.width
    const h = canvas.height

    const bs = 50
    const br = 10
    const wt = 2

    let counter = 0

    // ----
    // maze

    const maze = []
    const path = []

    const startPos = {
      x: Math.floor((Math.random() * w) / bs),
      y: Math.floor((Math.random() * h) / bs),
    }

    const generateMaze = () => {
      for (let y = 0; y < h / bs; y++) {
        maze.push([])

        for (let x = 0; x < w / bs; x++) {
          maze[y].push({ coords: { x, y } })
        }
      }

      const getAdjacent = (block) => {
        const x = block.coords.x
        const y = block.coords.y

        const adjacent = [
          maze[y - 1]?.[x],
          maze[y]?.[x + 1],
          maze[y + 1]?.[x],
          maze[y]?.[x - 1],
        ]

        return adjacent
      }

      const expand = (block) => {
        block.counter = counter
        counter += 1
        const neighbours = getAdjacent(block)
        const mappedNeighbours = neighbours.map((item, i) => {
          if (item === undefined) return null
          if (item.exits) return null
          return i
        })
        const validNeighbours = mappedNeighbours.filter((item) => item !== null)
        const direction =
          validNeighbours[Math.floor(Math.random() * validNeighbours.length)]

        if (validNeighbours.length) {
          if (neighbours[direction]) {
            block.exits
              ? block.exits.push(direction)
              : (block.exits = [direction])

            neighbours[direction].exits = [
              direction === 1 ? 3 : Math.abs(direction - 2),
            ]

            expand(neighbours[direction])
          }
        } else {
          if (counter < 10) {
            counter = 0
            maze.length = 0
            generateMaze()
          }
        }
      }
      expand(maze[startPos.y][startPos.x])
    }

    generateMaze()

    // draw maze
    const drawMaze = () => {
      const flatMaze = maze.flat()
      flatMaze.forEach((item) => {
        if (item.exits) path.push(item)
      })

      mazeCtx.fillStyle = '#000'
      mazeCtx.fillRect(0, 0, w, h)

      path.forEach((item) => {
        const x = item.coords.x * bs
        const y = item.coords.y * bs

        setTimeout(() => {
          mazeCtx.clearRect(x + wt, y + wt, bs - wt * 2, bs - wt * 2)

          item.exits.forEach((exit) => {
            if (exit === 0) mazeCtx.clearRect(x + wt, y, bs - wt * 2, wt)

            if (exit === 1)
              mazeCtx.clearRect(x + bs - wt, y + wt, wt, bs - wt * 2)

            if (exit === 2)
              mazeCtx.clearRect(x + wt, y + bs - wt, bs - wt * 2, wt)

            if (exit === 3) mazeCtx.clearRect(x, y + wt, wt, bs - wt * 2)
          })
        }, item.counter * 10)
      })
    }

    drawMaze()

    // ----
    // ball
    const pos = { x: startPos.x * bs + bs / 2, y: startPos.y * bs + bs / 2 }
    const v = { x: 0, y: 0 }

    // sensor
    const o = { x: 0, y: 0 }

    if (
      DeviceMotionEvent &&
      typeof DeviceMotionEvent.requestPermission === 'function'
    ) {
      DeviceMotionEvent.requestPermission()
    }

    window.addEventListener(
      'deviceorientation',
      (e) => {
        o.x = e.gamma
        o.y = e.beta

        if (o.x < -br) o.x = -br
        if (o.x > br) o.x = br
        if (o.y < -br) o.y = -br
        if (o.y > br) o.y = br
      },
      true
    )

    // render

    const tick = () => {
      requestAnimationFrame(tick)

      // update position
      v.x += o.x / 20
      v.y += o.y / 20

      if (Math.abs(v.x) > Math.abs(o.x)) v.x = o.x
      if (Math.abs(v.y) > Math.abs(o.y)) v.y = o.y

      pos.x += v.x
      pos.y += v.y

      if (pos.x < br) {
        pos.x = br
        v.x = v.x / -2
      }
      if (pos.x > w - br) {
        pos.x = w - br
        v.x = v.x / -2
      }
      if (pos.y < br) {
        pos.y = br
        v.y = v.y / -2
      }
      if (pos.y > h - br) {
        pos.y = h - br
        v.y = v.y / -2
      }

      // wall check

      path.forEach((item) => {
        const px = item.coords.x * bs
        const py = item.coords.y * bs
        const bx = pos.x
        const by = pos.y

        const num = document.getElementById('num')

        if (bx > px && bx < px + bs && by > py && by < py + bs) {
          num.innerText = `[${px}, ${py}]`

          if (item.exits.indexOf(0) === -1) {
            if (by - br < py + wt) {
              v.y = v.y / -2
              pos.y = py + br + wt
            }
          }

          if (item.exits.indexOf(1) === -1) {
            if (bx + br > px + bs - wt) {
              v.x = v.x / -2
              pos.x = px + bs - br - wt
            }
          }

          if (item.exits.indexOf(2) === -1) {
            if (by + br > py + bs + wt) {
              v.y = v.y / -2
              pos.y = py + bs - br - wt
            }
          }

          if (item.exits.indexOf(3) === -1) {
            if (bx - br < px + wt) {
              v.x = v.x / -2
              pos.x = px + br + wt
            }
          }
        }
      })

      // render
      ctx.fillStyle = '#fff'
      ctx.clearRect(0, 0, w, h)
      ctx.beginPath()
      ctx.arc(pos.x, pos.y, 10, 0, Math.PI * 2)
      ctx.fill()
    }

    tick()
  }

  return (
    <div className='App'>
      <canvas
        className={'maze'}
        ref={mazeRef}
        width={window.innerWidth}
        height={window.innerHeight}
      />
      <canvas ref={ref} width={window.innerWidth} height={window.innerHeight} />

      <h1 id='num'>Num</h1>

      <button onClick={start}>Start</button>
    </div>
  )
}

export default App
