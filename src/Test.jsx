import { useRef } from 'react'
import './App.scss'

function App() {
  const ref = useRef()

  const start = () => {
    // canvas
    const canvas = ref.current
    const ctx = canvas.getContext('2d')
    const w = canvas.width
    const h = canvas.height

    // ball
    const pos = { x: w / 2, y: h / 2 }

    // sensor
    const o = { x: 0, y: 0 }

    if (
      DeviceMotionEvent &&
      typeof DeviceMotionEvent.requestPermission === 'function'
    ) {
      DeviceMotionEvent.requestPermission()
    } else {
      alert('nope')
    }

    window.addEventListener(
      'deviceorientation',
      (e) => {
        o.x = e.gamma
        o.y = e.beta
      },
      true
    )

    // render

    const tick = () => {
      requestAnimationFrame(tick)

      // update position
      pos.x += o.x * 2
      pos.y += o.y * 2

      if (pos.x < 10) pos.x = 10
      if (pos.x > w - 10) pos.x = w - 10
      if (pos.y < 10) pos.y = 10
      if (pos.y > h - 10) pos.y = h - 10

      // render
      ctx.clearRect(0, 0, w, h)
      ctx.beginPath()
      ctx.arc(pos.x, pos.y, 10, 0, Math.PI * 2)
      ctx.fill()
    }

    tick()
  }

  return (
    <div className='App'>
      <canvas ref={ref} width={window.innerWidth} height={window.innerHeight} />

      <button onClick={start}>Start</button>
    </div>
  )
}

export default App
